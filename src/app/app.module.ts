import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './grid/grid.component';
import { ModulosService } from './services/modulos.service';
import { HttpClientModule } from '@angular/common/http';

import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
   declarations: [
      AppComponent,
      NavbarComponent,
      ParentComponent,
      ChildComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      GridModule,
      HttpClientModule,
      BrowserAnimationsModule,
      NgxLoadingModule.forRoot({})
   ],
   providers: [ModulosService],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
