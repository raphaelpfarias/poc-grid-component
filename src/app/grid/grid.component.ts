import { Component, OnInit, Input } from '@angular/core';
import { GridDataResult, SelectableSettings, DataStateChangeEvent, PageChangeEvent } from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class ChildComponent implements OnInit {
  // Recebe os dados que serão tratados com o process do kendo para serem exibidos na grid.
  @Input() gridData: GridDataResult;

  // Salva os dados da resposta, para fazer a paginação e filtragem localmente.
  @Input() gridDataInput: any[] = [];

  // Recebe o formato do cabeçalho da grid, como nome, width, filtrable ou se é hidden.
  @Input() gridSchema = [];

  // Variavel responsavel por exibir o loading.
  @Input() loading = false;

  // Configurações do modo de seleção das linhas da tabela
  @Input() selectableSettings: SelectableSettings;

  mySelection: number[] = [];

  state: State = {
    skip: 0,
    take: 10,
  };

  setSelectableSettings() {
    return this.selectableSettings;
  }

  constructor(
  ) {
    this.setSelectableSettings();
  }

  ngOnInit() { }

  cellClick(event) {
   // console.log(event.dataItem);
    //  this.idGrupo.emit(event.dataItem.id);
  }

  dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gridData = process(this.gridDataInput, this.state);
  }

  paginarGrid(evento: PageChangeEvent) {
    this.state.skip = evento.skip;
    this.state.take = evento.take;

    this.gridData = process(this.gridDataInput, this.state);
  }

}
