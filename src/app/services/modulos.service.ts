import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'Basic ' + btoa('engine:engine')
  })
};

@Injectable()
export class ModulosService {
  idUsuarioLogado;

  constructor(private http: HttpClient) {
    this.idUsuarioLogado = 1;
  }

  listarGrupos(): Observable<any> {
    return this.http.get<any>(environment.caminhoApi + `grupo?idUsuarioLogado=${this.idUsuarioLogado}`, httpOptions);
  }

}
