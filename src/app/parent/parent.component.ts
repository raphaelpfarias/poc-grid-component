import { Component, OnInit } from '@angular/core';
import { ModulosService } from '../services/modulos.service';
import { ColumnSettings } from '../interfaces/columnSettings';
import { SelectableSettings } from '../interfaces/selectableSettings';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  gridData = [];

  gridSchema: ColumnSettings[] = [
    {
      field: 'nome',
      title: 'Nome',
      width: '15%',
      filterable: true,
      type: 'text',
    },
    {
      field: 'descricao',
      title: 'Descrição',
      width: '15%',
      type: 'text'
    },
    {
      field: 'dthCadastro',
      title: 'Cadastrado em',
      width: '10%',
      type: 'date'
    },
    {
      field: 'flagAtivo',
      title: 'Ativo',
      width: '5%',
      type: 'boolean'
    },
    {
      field: 'id',
      title: 'ID',
      width: '5%',
      hidden: true,
      type: 'numeric'
    }
  ];

  loading = false;
  gridDataInput = [];

  selectableSettings: SelectableSettings = {
    checkboxOnly: false,
    mode: 'multiple',
    enabled: true
  };


  constructor(private moduloService: ModulosService) { }

  ngOnInit() {
    this.listarGrupos();
  }

  listarGrupos() {
    this.loading = true;
    this.moduloService.listarGrupos().subscribe(
      res => {
        this.gridData = res;
        this.gridDataInput = res;
        this.loading = false;

        console.log(res);
      },
      err => {
        this.loading = false;
        console.log(err);
      }
    );
  }

}
