import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParentComponent } from './parent.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ParentComponent]
})
export class ParentModule { }
